package com.example.motosharing.ui.home;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.motosharing.MainActivity;
import com.example.motosharing.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private TextView email;
    private FirebaseAuth auth;

    @SuppressLint("SetTextI18n")
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
         email = root.findViewById(R.id.text_home);
         Button btn = root.findViewById(R.id.button);


        auth = FirebaseAuth.getInstance();
        FirebaseUser user = auth.getCurrentUser();

        if (user!=null)
        email.setText(user.getEmail());
        else {
            startActivity(new Intent(getContext(), MainActivity.class));
            email.setText("no login");
        }

        btn.setOnClickListener(v -> {
            auth.signOut();
            if (user!=null)
            email.setText(user.getEmail());
            else email.setText("logout");
        });


        return root;
    }


    /*@Override
    public void onStart() {
        super.onStart();
        FirebaseUser mFirebaseUser = mFirebaseAuth.getCurrentUser();
        if (mFirebaseUser!=null){
            email.setText(mFirebaseUser.getEmail());
        }else{
            email.setText("non c'è nessuno loggato");
        }
    }*/


}